# Agile Buzzwords


| Scrum Term | German | Explanation | Alternative/Agile Terms | Links |
| ------ | ------ | ------ | ------ | ------ |
| Acceptance Criteria | Akzeptanzkriterium | Überprüfbare Kundenanforderungen in einer User-Story | ... | ... |
| Burndown Chart | Fortschritts-Diagramm | ... | ... | ... |
| Boards | Tafeln | Um Post-It zu Gruppieren | ... | ... |
| Daily Standup Meeting | Tägliches Treffen | ... | ... | ... |
| Incident | Zwischenfall | ... | Problem | ... |
| Labels | Beschriftungen | Zwecks Kenntzeichnung und Gruppierung | Etikette | ... |
| Milestone | Meilensteine | ... | ... | ... |
| Product Increment | Neue Produkt-Merkmale | ... | Prototyp | ... |
| PBI | Produkt Merkmale | Product Backlog Item | Todo, Issue, Post-It-Zettel | ... |
| Planning Poker | Planungspoker | ... | ... | ... |
| [Product Backlog](img/pb.png) | Produkt-Merkmal-Liste | ... | ... | ... |
| Product Owner | Produktbesitzer | Produktverantwortlicher zwischen Kunde und Entwickler | ... | ... |
| Scrum Master | Scrum Prozessverantwortlicher | ... | ... | ... |
| Scrum Team | Entwickler-Gruppe | Owner, Master, Developer | ... | ... |
| Sprint Retrospektive | Interner Rückblick | Product Increment (neuer Prototyp) mit Team besprechen | ... | ... |
| Sprint Review | Externer Rückblick | Product Increment (neuer Prototyp) mit Kunde oder Product Owner besprechen | ... | ... |
| Sprint | Meileinstein, Unterteilung, Kurzziel, Wiederholung | Short release cycle | Iteration, Milestone | ... |
| Story Points | "User Story"-Punkte | ... | Weight | ... |
| Task | Aufgabe | Todo für Entwickler | ... | [Task vs Userstory](https://www.agile-academy.com/de/product-owner/der-unterschied-zwischen-story-und-task/) |
| User Story | Benutzergeschichte | Kundenwunsch: "Wer will was und warum" allgemeinverständlich formuliert. Vom Produkt Owner verantwortet. | ... | ... |
| Waterfall model | Wasserfallmodell | ... | ... | ... |

## Othere Buzzwords

| Term | German | Explanation | Alternative/Agile Terms | Links |
| ------ | ------ | ------ | ------ | ------ |
| Repository | Aufbewahrungsort | ... | ... | ... |
| Pull | ziehen | ... | ... | ... |
| Push | drücken | ... | ... | ... |
| Merge | Zusammenführen | ... | ... | ... |
| Complaint | Reklamation | ... | Beschwerde | ... |
| Contributors | Mitwirkende | ... | ... | ... |
| Maintainers | Verantwortlicher, Verwalter | ... | ... | ... |
| Requests | Gesuche | ... | ... | ... |
| Expired | abgelaufen | ... | ... | ... |
